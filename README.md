# ENKI-cluster-config

This project provides CI/CD files for configuring and deploying an ENKI server in a Kubernetes cluster on Google Cloud. This workflow automates deployment of the ENKI cluster service.

The project is based on a prototype [example cluster applications project](https://gitlab.com/gitlab-org/cluster-integration/example-cluster-applications/) developed on GitLab.

The CI/CD pipeline build and deployment workflow of this project is triggered on the master branch when a pipeline successfully completes for a new tag on the master branch of the ENKI-portal/ThermoEngine project.

## Contents  
[- Installation](#installation)  
[- Tearing down the cluster](#tearing-down-the-cluster)  
[- Customization](#customization)  

## Installation
Configuration of the ENKI cluster requires creation of a [Google Cloud](https://console.cloud.google.com/home/dashboard) project and localization of this GitLab repository. The following workflow guides you through this process.

### 1️⃣ Configure Google Cloud (GKE) 
1. In the [Google Cloud Console](https://console.cloud.google.com/home/dashboard), create a new billing account, or log in with your existing Google Cloud billing account if you have one (upper right account icon).
1. Use the dropdown menu at the top left of the Console to create a new project.
1. Navigate to the Compute Engine configuration page of the new project in order to initialize project resources. *Do not* create any resources using the Google Cloud Console.
1. Because the cluster requires a dedicated external IP address to be associated with a server name configured at your DNS provider,  create a static IP address for this project:
    1. Go to the [VPC Network > External IP addresses](https://console.cloud.google.com/networking/addresses) pane for your newly created project.  
    1. Click **Reserve Static Address**.  
    1. Name the address, e.g., **enki-server**.
    1. Describe the address, e.g., *IP address for the JupyterHub Ingress load balancer suporting the ENKI GKE cluster*.
    1. Select **Premium**, **IPv4**, **Regional**.
    1. Select a **Region** where the Google cluster will be physically situated. This region will be used when the cluster is instantiated by the GitLab project. Choose a region appropriate for access by the majority of your users, e.g., **us-west1-a** or **us-central1**.
    1. Leave the **Attached to** dropdown menu at **None**.
    1. Click to reserve the address. An address is assigned.
    1.   Note the address assigned.  In the remaining instructions below, this address is referred to as the **External IP** cluster address.
    
### 2️⃣ Configure DNS Provider  
1. Log into the website of your DNS provider where you purchased your domain name for the cluster (e.g., [GoDaddy](https://www.godaddy.com)).
2. Navigate to the DNS configuration settings for your domain (e.g., *enki-portal.org*).
3. Create a DNS **[A record](https://support.dnsimple.com/articles/a-record/)** so that the server domain name (e.g., *server.enki-portal.org*) points to the **External IP** address.   
   This action associates the static address with the symbolic domain name.
   
### 3️⃣ Wait for propagation
Go for a walk or have a coffee. ☕️ Domain name propagation to the Internet router system requires up to an hour, and this propagation MUST be completed prior to the next steps in the workflow.

- Make sure that the router lookup tables have propagated. You can use a utility such as **Network Utility** on MacOS for this task.

### 4️⃣ Finish configuration in this GitLab project

1. **In this GitLab project, create the Google Cloud Kubernetes cluster and allow GitLab to manage it:** 

   1. In this GitLab project, go to **Operations > Kubernetes**.  
   1. Click **Add Kubernetes cluster**.
   1. Click **Create new cluster** and then click **Google GKE**.
   1. Log in as the owner of the billing account on Google Cloud if prompted.
   1. Name the cluster (lowercase, no spaces or punctuation).
   1. Select the project that you just created at Google Cloud from the dropdown menu.
   1. Select the zone and number of nodes for the cluster. Use the same zone (region) that you chose above when configuring the static **External IP**.  
      - Suggestion: machine type, n1-standard-2
      - Suggestion: number of nodes, 2
      - Suggestion: zone, us-west1-a
   1. Set the cluster to be managed by GitLab.
   1. Click the **Create Kubernetes cluster** button. Creation will take a few moments. Go for another walk or have another coffee. ☕️
2. **Set up authentication mechanisms so that GitLab users can log into the cluster:** 

   1. Navigate to your user account settings (dropdown menu associated with your user icon in the upper right corner of the screen)
   1. Select **Applications**, and add a new application with the following:
      - Name of choice
      - Redirect URL of the form http://server.enki-portal.org/hub/oauth_callback, with your domain name substituted
      - Grant **api**, **read_repository**, and **write_repository** scope
   1. Save and click the newly created oauth token to obtain the **Application ID** and **Secret**, which you will need below.  
   
3. **Configure environment variables for the CI/CD pipeline cluster build:** 

   1. Go to **Settings > CI/CD > Variables** in the project settings in order to initialize JupyterHub environment variables.
   1. Create and save the following environment variables. Make sure to mask the secrets and tokens so that they are not visible in public logs of the pipeline process.
       - JUPYTERHUB_PROXY_SECRET_TOKEN  
        Sets proxy.secretToken. Generate by running the command ```openssl rand -hex 32``` in a terminal shell.
       - JUPYTERHUB_COOKIE_SECRET  
        Sets hub.cookieSecret. Generate using ```openssl rand -hex 32```.
       - JUPYTERHUB_HOST  
        Hostname used for the installation. For example, *server.enki-portal.org*.
       - JUPYTERHUB_GITLAB_HOST  
        Hostname of the GitLab instance used for authentication. For example, *gitlab.com*.
       - JUPYTERHUB_AUTH_CRYPTO_KEY  
        Sets auth.state.cryptoKey. Generate using ```openssl rand -hex 32```.
       - JUPYTERHUB_AUTH_GITLAB_CLIENT_ID  
        **Application ID** for the OAuth Application generated above
       - JUPYTERHUB_AUTH_GITLAB_CLIENT_SECRET  
        **Secret** for the OAuth Application generated above
       - JUPYTERHUB_LOAD_BALANCER_IP  
        **External IP** address obtained above  
         
4. **Configure yaml configuration files.**  
These edits should be performed on a local clone of the repository and then, after all the edits are committed, pushed back to the remote. Alternatively, you could make a branch of master at the GitLab IDE, make and commit edits on that branch, and subsequently merge that branch back into master.  The critical thing is that all the edits must be made *before* the CI/CD pipeline executes in the next step; that pipeline is triggered by any commit to the master branch.   The localizations required in these configuration files are redundant with respect to the environment variables set above. Unfortunately, the current implementation of GitLab's managed cluster services requires this redundancy.  Hopefully, this situation will change in the near future.  

   1. Edit the file *.gitlab/managed-apps/ingress/values.yaml* by setting the key value of **loadBalancerIP** to reflect the **External IP** address obtained above:   
       ```
       controller:
       replicaCount: 1
       service:
       loadBalancerIP: 35.202.218.17  
       ```  
   2. Edit the file *.gitlab/managed-apps/jupyterhub/values.yaml* to update the following key values:
      - ```hub: extraEnv: GITLAB_HOST: JUPYTERHUB_GITLAB_HOST```
      - ```hub: extraEnv: GITLAB_URL: https://JUPYTERHUB_GITLAB_HOST```
      - ```hub: cookieSecret: JUPYTERHUB_COOKIE_SECRET```
      - ```auth: state: cryptoKey: JUPYTERHUB_AUTH_CRYPTO_KEY```
      - ```auth: gitlab: clientId: JUPYTERHUB_AUTH_GITLAB_CLIENT_ID```
      - ```auth: gitlab: clientSecret: JUPYTERHUB_AUTH_GITLAB_CLIENT_SECRET```
      - ```singleuser: extraEnv: GITLAB_HOST: JUPYTERHUB_GITLAB_HOST```
      - ```auth: gitlab: callbackUrl: http://JUPYTERHUB_HOST/hub/oauth_callback```
      - ```ingress: hosts: JUPYTERHUB_HOST```
      - ```ingress: tls: hosts: JUPYTERHUB_HOST```
      - ```proxy: secretToken: JUPYTERHUB_PROXY_SECRET_TOKEN```  

   A copy of the default localized JupyterHub yaml file is provided [at the end of this README](#specific-jupyterhub-customizations).

5. **Link this cluster build project so that the CI/CD pipeline executes automatically when the pipeline for the ThermoEngine project completes.** 

   1. Go to **CI/CD > Pipeline subscriptions**.  
   1. Enter **ENKI-portal/ThermoEngine** as project path, and click **Subscribe**.  
Now, every time a **tagged** commit to the **master** branch of **ThermoEngine** is pushed and that pipeline is executed successfully causing a new Docker image of the ENKI-portal to be created, the pipeline for this project is executed and that image is propagated to the cluster.  

### 5️⃣ Run the CI/CD Pipeline
- Navigate to **CI/CD > Pipelines** and run the pipeline.  
This action configures the cluster with the following:
  - Helm and Tiller for package management
  - Ingress and LetsEncrypt for Internet access and https encryption
  - Prometheus for cluster monitoring
  - JupyterHub along with a custom container image for accessing the ENKI portal on the cluster. Note that the custom image installed on the hub for single user access is built by CI pipelines in the ThermoEngine project.

Every time you modify the configuration files in this project and commit the result, this pipeline runs automatically and updates the cluster with these changes.

## Tearing down the cluster
1. At the project IDE on GitLab, navigate to **Operations > Kubernetes** and click the cluster name.
1. On the **Advanced Settings** tab, click **Remove integration and resources**. 
1. Type the name of the cluster in the textbox, and click **Remove integration and resources**. The GitLab cluster integration files are removed.
1. Go to the [Google Cloud Console](https://console.cloud.google.com/home/dashboard), and navigate to **Kubernetes Engine**.
1. Click the trash icon for the cluster.  After several minutes, the cluster is removed.
1. Navigate to **VPC Network > External IP addresses** and choose the static **External IP** address that is no longer in use. Then click **Release static address** to free this resource.

## Customization
- Turn cluster applications on or off with flags in the .gitlab/managed-apps/config.yaml file.
- Customize applications by altering the corresponding values.yaml file within each application directory. For example, for JupyterHub the file is .gitlab/managed-apps/jupyterhub/values.yaml.

### Specific JupyterHub customizations  
The .gitlab/managed-apps/jupyterhub/values.yaml file defines localized JupyterHub configuration parameters for the ENKI cluster. In particular, note the following: 
- The single user JupyterLab image is custom and built by the **ENKI-portal/ThermoEngine** project on GitLab.
- The default JupyterHub login screen is replaced with a custom template maintained in the **ENKI-portal/jupyterhub_custom** project on GitLab. 
- The **gitlabProjectWhiteList** and **gitlabGroupWhiteList** values are overwritten, which means that the corresponding values in the *.gitlab/managed-apps/config.yaml* files are not referenced.
- gitpuller is used to create/update local copies of **ENKI-portal/ThermoEngine** and **ENKI-portal/enki-workshops** projects for each user pod.

Here is a current copy of the configuration file:
```
hub:
  extraEnv:
    JUPYTER_ENABLE_LAB: 1
    GITLAB_HOST: gitlab.com
    GITLAB_URL: https://gitlab.com
  cookieSecret: 4c8c750947ae40ba67357736197d81971b2391d821eb5050f238bd7cf9130e83
  extraConfig: |
    c.GitLabOAuthenticator.scope = ['api read_repository write_repository']
    c.JupyterHub.template_paths = ['/etc/custom/jupyterhub_custom/templates']

    async def add_auth_env(spawner):
      '''
      We set user's id, login and access token on single user image to
      enable repository integration for JupyterHub.
      See: https://gitlab.com/gitlab-org/gitlab-foss/issues/47138#note_154294790
      '''
      auth_state = await spawner.user.get_auth_state()

      if not auth_state:
          spawner.log.warning("No auth state for %s", spawner.user)
          return

      spawner.environment['GITLAB_ACCESS_TOKEN'] = auth_state['access_token']
      spawner.environment['GITLAB_USER_LOGIN'] = auth_state['gitlab_user']['username']
      spawner.environment['GITLAB_USER_ID'] = str(auth_state['gitlab_user']['id'])
      spawner.environment['GITLAB_USER_EMAIL'] = auth_state['gitlab_user']['email']
      spawner.environment['GITLAB_USER_NAME'] = auth_state['gitlab_user']['name']

    c.KubeSpawner.pre_spawn_hook = add_auth_env
  initContainers:
    - name: git-clone-templates
      image: alpine/git
      args:
        - clone
        - --single-branch
        - --branch=master
        - --depth=1
        - --
        - https://gitlab.com/ENKI-portal/jupyterhub_custom.git
        - /etc/custom/jupyterhub_custom
      securityContext:
        runAsUser: 0
      volumeMounts:
        - name: custom-templates
          mountPath: /etc/custom/jupyterhub_custom
  extraVolumes:
    - name: custom-templates
      emptyDir: {}
  extraVolumeMounts:
    - name: custom-templates
      mountPath: /etc/custom/jupyterhub_custom

auth:
  type: gitlab
  state:
    enabled: true
    cryptoKey: 4c8c750947ae40ba67357736197d81971b2391d821eb5050f238bd7cf9130e83
  gitlab:
    clientId: fd5d485c51e879692a6fe9a9d16c2581e79fc12925151e190e58c75515dad485
    clientSecret: 854d625aed3c33190d4cb491b4616feab82a727fb595c1920f4401f9a7c27d46
    callbackUrl: http://workshop.enki-portal.org/hub/oauth_callback
    gitlabProjectIdWhitelist: []
    gitlabGroupWhitelist: []
  admin:
    access: true
    users:
      - ghiorso

singleuser:
  extraEnv:
    GITLAB_HOST: gitlab.com
  defaultUrl: "/lab"
  image:
    name: registry.gitlab.com/enki-portal/thermoengine
    tag: latest
  lifecycleHooks:
    postStart:
      exec:
        command:
          - "sh"
          - "-c"
          - >
            gitpuller https://gitlab.com/enki-portal/ThermoEngine master ThermoEngine;
            gitpuller https://gitlab.com/ENKI-portal/enki_workshops.git master ENKI_Workshops

ingress:
  enabled: true
  hosts:
    - workshop.enki-portal.org
  tls:
    - hosts:
        - workshop.enki-portal.org
      secretName: "jupyter-cert"
  annotations:
    kubernetes.io/ingress.class: "nginx"
    kubernetes.io/tls-acme: "true"

proxy:
  secretToken: 4c8c750947ae40ba67357736197d81971b2391d821eb5050f238bd7cf9130e83
  service:
    type: ClusterIP
```

