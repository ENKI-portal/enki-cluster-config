# Changelog
All notable changes to this project are documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2020-04-14 (ghiorso)
### Added 
- README document
- CHANGELOG document
- Working Kubernetes cluster on Google Cloud

### Changed
- Default configuration files taken from [example cluster applications project](https://gitlab.com/gitlab-org/cluster-integration/example-cluster-applications/).  

*standard changelog tags: Added, Changed, Deprecated, Removed, Fixed, Security*